<?php

function carpetaArchivo() {
    //Capturamos la fecha en el formato deseado
    $fecha = date("Y-m-d H:i");
    
    //Creamos la carpeta con el nombre recogido en fecha
    mkdir("./" . $fecha);

    //Guardamos el nombre de la carpeta 
    $carpeta = "./" . $fecha;

    //Copiamos el archivo con el nombre modificado en la carpeta recién creada
    copy("elquijote.txt", $carpeta . "/elquijote.txt.modificado");
}

carpetaArchivo();

?>